#+title: Industry Trade and Agriculture

* Agricultural Reforms
These were all part of the Green Revolution initiated in the 1960's
** Zamindari System Abolished
- The Zamindari system was exploitative both for the farmers and the economy as well this was because land holdings were given in the hands of those that did not want to engage in agriculture and thus proper measures were not taken by these zamindars to improve the productivity and production of their fields. They only wanted to leach off the profit 
** Ceiling on Land holdings
- Ceiling on land holdings was an anti-monopoly sort of a move and thus it made sure that wealthy individuals donot not capture all the available land and gain monopoly over agricultural production 
** Consolidation on Holdings
- The problem of small farmer was that they had small holdings and fragmented ones which led to less production, to combat this lands were consolidated in a way that they create considerable acreage
** Regulation of Rent
- The problem of landless labourers was that they were supposed to work on the lands of the zamindars and also pay rent for allowing them to work in thier lands, this was an added economic burden on the farmers. This problem was removed with governmental regulations on rent which made it so that the cost of rents could not reach beyond 1/3rd of the total output
** Co-operative Farming 
- Co-operative farming was introduced into the villeges by the govt for the benefit of the small and medium farmers. This was because engaging in the collective sale of produce is much more profitable than individual sale by every member of the cop
** Use of HYV Seeds
- This an example of the technological changes that were brought, the use of HYV seeds leads to a higher yeild than the use of normal seeds which means that there is an increase in both the producitivity of agriculture and the monetary gains of the farmer per crop
** Chemical Fertilizers
- Altough now considered as dangerous and harmful they were considered essential when they were introduced because they improved the yeild of the crop by chemically altering the productivity of the soil
** Crop Protection
- The technology also now allowed farmers to protect their crops from other elements such as bugs and other bacteria that might cause harm to the enitre crop produce. This was through pesticides

* Problems with the Green Revolution (Above Mentioned Reforms)
- The GR only focussed on wheat and rice, the cultivators of other crops got nothing
- There was an uneven spread because considerable advantages could only be felt in UP Punjab and Haryana
- Socially also there was uneqal disribution because most of the implements brought out were expensive due to which many farmers could not afford them

* Industries
*Why did the government want to invest in the industrial sector*

- Lack of capital
- Lack of capital leads to less production leads to less employment less income and a slow market and thus negligible demand thus no incentive
- Fear of the formation of private monopolies that would exploit the poor

** The Solution : IPR 2 1956
The IPR stands for Industry Policy Resolution that was released in 1956, and the main idea presented here is that the public sector will be heavily investing in industrialisation of the economy and that the private institutions will have to go around legalities and other formalities in order to setup industries. This was done so that the number of private industries and thier control over the economy stays in control

*Salient Points*
- Division into 3 main types of industries
  - Industries where is there is only PSU
  - Industries that have PSU as well as private counter parts
  - Industries other than the 1st and the 2nd conditions
- Licensing for Private industries was created, the motive of that was to incentivise industries to open up in backward areas and support regional growth
- Concessions were provided to those industries that setup in backward areas
  - Tax Holidays
  - Cheap Electricity
- This IPR also focussed on the empowerment of SSI (Small Scale Industries)

** SSI
Some features
- They are employment friendly (labour intensive)
- They are mobile and require less investment
- Promote equality among lower economic classes

In order to preserve such industries the policy of Import Substitution was established and the domestic economy was closed in order to protect it from outside competition

** Import Substitution
- It refers to the idea of producing those goods that are imported in the domestic economy
- This was done for 2 reasons
  1. To reduce the number of imports so that domestic industries get the much needed push
  2. To conserve forex reserves for imports that cannot be substituted
- This was especially important since India was also not inviting large ammounts of foreign investment which could replish its forex reserves
* Some Issues with this Policy
There were some problems with this policy too
- The public institutions were becomming inefficient and thus became social dead weight
- The private industries were not given enough space to function and the public invaded their space
- There was hoardings and improper use of the license and permit system that was setup by the government
